var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function check() {
    var a = parseInt(t1.value);
    var b = parseInt(t2.value);
    var c = parseInt(t3.value);
    if (a == b && b == c) {
        document.getElementById("display").innerHTML = "Equilateral";
    }
    else if (a >= (b + c) || c >= (b + a) || b >= (a + c)) {
        document.getElementById("display").innerHTML = "Not a triangle";
    }
    else if ((a == b && b != c) || (a != b && c == a) || (c == b && c != a)) {
        document.getElementById("display").innerHTML = "Isosceles";
    }
    else if (a != b && b != c && c != a) {
        document.getElementById("display").innerHTML = "Scalene";
    }
}
//# sourceMappingURL=app.js.map