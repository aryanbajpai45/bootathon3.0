function find() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    var t3 = document.getElementById("t3");
    var s = t1.value;
    var i = s.indexOf('+');
    if (i == -1)
        i = s.indexOf('-');
    var j = s.indexOf("i");
    if (i == -1) {
        if (j == -1) {
            t2.value = s;
            t3.value = "0";
        }
        else {
            t2.value = "0";
            t3.value = s.substring(0, j);
        }
    }
    else {
        t2.value = s.substring(0, i);
        t3.value = s.substring(i + 1, j);
    }
}
//# sourceMappingURL=app.js.map